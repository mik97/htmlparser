﻿''' <summary>
''' Classe Balise
''' </summary>
Public Class Balise: Inherits TreeNode
    Public Property Contenu() As String
    Public Property NomBalise() As String
    Public Property Attributs() As Dictionary(Of String, String)

    Public Sub New(pContenu As String, pNomBalise As String, pAttributs as Dictionary(Of String, String))
        Contenu = pContenu
        NomBalise = pNomBalise
        Attributs = pAttributs
        Text = "<" + NomBalise + ">"
    End Sub
    ''' <summary>
    ''' ToString() De la classe balise retourne la balise formaté en format HTML
    ''' </summary>
    ''' <returns></returns>
    Public Overrides Function ToString() As String
        Dim chaineAttributs As String =""
        Dim i As Integer = 0
        For Each elem In Attributs
            chaineAttributs += (" " + elem.Key + "=") & Chr(34) & elem.Value & Chr(34)
            i += 1
        Next

        Return ("<" & NomBalise.ToLower() & chaineAttributs + ">" + Contenu)
    End Function
End Class
