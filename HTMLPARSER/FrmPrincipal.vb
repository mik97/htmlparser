﻿Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary

Public Class FrmPrincipal
    Private docHtml As String
    Private noeudRacine As Balise
    Private noeudCourant As Balise 
    Private listeFile As Queue(Of String)
    Private sw As StreamWriter

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub
    ''' <summary>
    ''' Charger un fichier permet à partir d'un fichier .txt ou .html
    ''' de transformer le fichier en structure d'object
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ChargerFichierToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ChargerFichierToolStripMenuItem1.Click
        Dim ofd As New OpenFileDialog
        ofd.Filter = "html files (*.html)|*.html|txt files (*.txt*)|*.txt*"
        ofd.DefaultExt = ".html"
        If ofd.ShowDialog = DialogResult.OK Then
            Try
                Dim unFichier As New FileStream(ofd.FileName, FileMode.Open)
                Dim sr As StreamReader = New StreamReader(unFichier)
                Dim fichierTexte As String = sr.ReadToEnd().Replace(vbCrLf, "")
                listeFile =  New Queue(Of String)(fichierTexte.Trim().Split("<"))
                listeFile.Dequeue()

                trArbreBalises.Nodes.Clear()
                noeudRacine = CreerBalise(listeFile.Dequeue)
                CreerArbre(noeudRacine)
                trArbreBalises.Nodes.Add(noeudRacine)
                trArbreBalises.SelectedNode = trArbreBalises.TopNode

                sr.Close()
                unFichier.Close()
                AffFonctionBase(True)
            Catch ex As Exception
                lblChargementFichier.Text = "Le document n'a pas pu être chargé"
                lblChargementFichier.ForeColor = Color.Red
                lblChargementFichier.Visible = True
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Exportation de fichiers en format HTML 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub ExporterFichierToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExporterFichierToolStripMenuItem.Click
        Dim sfd As New SaveFileDialog
        sfd.Filter = "html files (*.html)|*.html"
        sfd.DefaultExt = ".html"
        If sfd.ShowDialog() = DialogResult.OK Then
            lblChargementFichier.Visible = True
            Try
                sw = New StreamWriter(sfd.FileName,False)
                docHtml = ""
                TransformerEnFichier(noeudRacine)
                sw.Write(docHtml)
                sw.Close()
                lblChargementFichier.Text = "L'exportation c'est effectuée avec succès!"
                lblChargementFichier.ForeColor = COlor.green    
            Catch ex As Exception
                lblChargementFichier.Text = "L'exportation ne s'est pas effectuée"
                lblChargementFichier.ForeColor = COlor.Red    
            End Try
        End If
    End Sub
    ''' <summary>
    ''' La méthode crée un arbre de Balise
    ''' </summary>
    ''' <param name="pNoeud"></param>
    Private Sub CreerArbre(pNoeud As Balise) 
        If listeFile.Peek.StartsWith("/")
            noeudCourant = pNoeud.Parent
            listeFile.Dequeue()
        ElseIf Not listeFile.Peek.LastIndexOf(">") = listeFile.Peek.Count -1
            AjouterNoeud(pNoeud, listeFile.Peek)
        Else
            AjouterNoeud(pNoeud, listeFile.Peek.Replace(">", " "))
            While listeFile.Count -1 > 0
                CreerArbre(noeudCourant)
            End While
        End If
    End Sub

    ''' <summary>
    ''' AjouterNoeud(..) Permet d'ajouter un noeud(balise) dans l'abre de Noeud
    ''' </summary>
    ''' <param name="pNoeud"></param>
    ''' <param name="infoBalise">Information qui va permettre de créer une balise</param>
    Private Sub AjouterNoeud(pNoeud As Balise, infoBalise As String)
        pNoeud.Nodes.Add(CreerBalise(infoBalise))
        noeudCourant = pNoeud.LastNode
        listeFile.Dequeue()
    End Sub

    ''' <summary>
    ''' La méthode crée une balise à l'aide du paramètre recus en paramètre
    ''' ajoute les attributs dans un dictionnaire ou ajoute le contenu de la 
    ''' balse dans une chaine de caractères.
    ''' </summary>
    ''' <param name="infoBalise">Chaine de caractèresqu'on veut transformer
    ''' en une Balise</param>
    ''' <returns></returns>
    Private Function CreerBalise(infoBalise As String) As Balise
        infoBalise = infoBalise.Replace(">", " ").Trim()
        Dim liste As List(Of String) = infoBalise.Split(" ").ToList
        Dim lstAttributs As List(Of String)
        Dim contenu As String = ""
        Dim DicoAttributs As New Dictionary(Of String, String)

        For index = 1 To liste.Count -1
            If liste(index).Contains("=")
                lstAttributs = (New List(Of String)(liste(index).Split("""")))
                DicoAttributs.Add(lstAttributs.First.Replace("=", ""), lstAttributs(1))
            Else
                contenu += liste(index) + " "
            End If
        Next

        Return New Balise(contenu.Trim(),liste.First,DicoAttributs)
    End Function

    ''' <summary>
    ''' Méthode qui permet de supprimer si elle existe la balise sélectionnée
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnSupprimer_Click(sender As Object, e As EventArgs) Handles btnSupprimer.Click
        MasquerInfo()
        If trArbreBalises.SelectedNode IsNot Nothing
            If lboResultatRechercheBalise.Items.Contains(Ctype(trArbreBalises.SelectedNode, Balise))
                lboResultatRechercheBalise.Items.Remove(Ctype(trArbreBalises.SelectedNode, Balise))
            End If
            trArbreBalises.SelectedNode.Remove()

            If trArbreBalises.TopNode Is Nothing
                AffFonctionBase(False)
            End If

            trArbreBalises.Refresh()
            AffErreurBalise(False)
        Else
            AffErreurBalise(True)
        End If
    End Sub

    ''' <summary>
    ''' Permet de faire la gestion du programme, il affiche ou non différentes
    ''' fonctions basiques du programme
    ''' </summary>
    ''' <param name="booleen"></param>
    Private Sub AffFonctionBase(booleen As Boolean)
        btnRechercherBalise.Enabled = booleen
        btnRechercheId.Enabled = booleen
        txtNomRecherche.Enabled = booleen
        lblChargementFichier.Visible = false
        lboResultatRechercheBalise.Items.Clear
    End Sub
    ''' <summary>
    ''' Permet d'ajouter une balise à partir du noeud sélectionné
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnAjouterBalise_Click(sender As Object, e As EventArgs) Handles btnAjouterBalise.Click
        MasquerInfo()
        If trArbreBalises.SelectedNode IsNot Nothing
            if Not String.IsNullOrWhiteSpace(txtNomBalise.Text)
                trArbreBalises.SelectedNode.Nodes.Add(CreerBalise(txtNomBalise.Text))
                txtNomBalise.Text = ""
                AffErreurBalise(false)
            Else
                AffErreurBalise(True)
                lblInformation.Text = "Vous devez inscire un nom balise!"
            End If
        Else
            AffErreurBalise(true)
        End If
    End Sub
    ''' <summary>
    ''' Permet de modifier les informations d'une balise.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BtnModifier_Click(sender As Object, e As EventArgs) Handles btnModifier.Click
        ModifierBalise()
        MasquerInfo()
    End Sub

    ''' <summary>
    ''' La méthode permet de rechercher une balise spécifique à l'aide d'un critère de recherche
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnRechercherBalise_Click(sender As Object, e As EventArgs) Handles btnRechercherBalise.Click
        lboResultatRechercheBalise.Items.Clear()
        If Not String.IsNullOrEmpty(txtNomRecherche.Text)
            RechercheSpecifique(noeudRacine, txtNomRecherche.Text,1)
            AffErreurRecherche(False)
        Else
            AffErreurRecherche(True)
        End If
    End Sub
    ''' <summary>
    ''' Méthode utilise après une fonction de recherche, permet de soit afficher
    ''' des messages d'erreur ou sinon de sélectionner la premiere balise
    ''' </summary>
    ''' <param name="bool"></param>
    Private Sub AffErreurRecherche(bool As Boolean)
        If lboResultatRechercheBalise.Items.Count > 0
            lboResultatRechercheBalise.SelectedIndex = 0
            btnEffacer.Enabled = True
            MasquerInfo()
        Else
            btnTrouverElem.Enabled = False

            If String.IsNullOrEmpty(txtNomRecherche.Text)
                lblErreurRecherche.Text = "Vous devez spécifier un mot à rechercher"
            Else
                lblErreurRecherche.Text = "Aucun résultat n'a été trouvé"
            End If
            Dim lblInformation1 As Label = lblInformation
            lblInformation1.Visible = False
            lblErreurRecherche.Visible = True
        End If
    End Sub
    ''' <summary>
    ''' La méthode permet de faire une recherche spécifique dans l'arbre
    ''' Dans le cas ou typerecherche = 1, la recherche fait une recherche par nom de balise
    ''' Sinon elle fait une recherche par id d’un élément dans le document 
    ''' </summary>
    ''' <param name="pNoeud">Noeud dans lequel on veut parcourir pour effectuer la recherche</param>
    ''' <param name="valeurRecherche">La valeur qui est recherché</param>
    ''' <param name="typeRecherche">Type de recherche qu'on veut faire</param>
    Private Sub RechercheSpecifique(pNoeud As Balise, valeurRecherche As String, typeRecherche As Integer)
        valeurRecherche = valeurRecherche.replace("<","").Replace(">","").Trim()
        If pNoeud IsNot Nothing
            Select Case typeRecherche
                Case 1
                    If pNoeud.NomBalise.StartsWith(valeurRecherche)
                        lboResultatRechercheBalise.Items.Add(pNoeud)
                    End If
                Case Else
                    If pNoeud.Attributs.ContainsKey("id")
                        If pNoeud.Attributs("id").StartsWith(valeurRecherche)
                            lboResultatRechercheBalise.Items.Add(pNoeud)
                        End If
                    End If
            End Select

            For Each noeud As Balise In pNoeud.Nodes
               RechercheSpecifique(noeud, valeurRecherche, typeRecherche)
            Next
        End If
    End Sub

    ''' <summary>
    ''' La méthode transforme toutes les balises qui ont été chargé en un document html.
    ''' </summary>
    ''' <param name="pNoeud">>Noeud dans lequel on veut parcourir pour écrire dans le document</param>
    Private Sub TransformerEnFichier(pNoeud As Balise)
        docHtml += pNoeud.ToString()
        If pNoeud.Nodes.Count = 0
            docHtml += "</" + pNoeud.NomBalise + ">"
        End If

        For Each elem  In pNoeud.Nodes
            TransformerEnFichier(elem)
        Next

        If pNoeud.Parent IsNot Nothing
            If pNoeud.Parent.LastNode.ToString = pNoeud.ToString
                docHtml += "</" +CType(pNoeud.Parent, Balise).NomBalise + ">" 
            End If
        End If
    End Sub

    ''' <summary>
    ''' Permet d'afficher le contenu de chaque balise
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub TreeView1_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles trArbreBalises.AfterSelect
        rtxContenuBalise.Text = ""
        dgvAffAttributContenu.Rows.Clear()
        If Not trArbreBalises.SelectedNode Is Nothing
            affichageInfoBalise()
            noeudCourant = trArbreBalises.SelectedNode
            For Each elem In noeudCourant.Attributs
                dgvAffAttributContenu.Rows.Add(elem.Key,elem.Value)
            Next
            rtxContenuBalise.Text = noeudCourant.Contenu

            trArbreBalises.SelectedNode.BackColor = System.Drawing.SystemColors.Highlight
            trArbreBalises.SelectedNode.ForeColor = System.Drawing.SystemColors.HighlightText
        End If
    End Sub

    Private SUb affichageInfoBalise()
        rtxContenuBalise.Enabled = True
        dgvAffAttributContenu.Enabled = True
        btnAjouterBalise.Enabled = True
        btnSupprimer.Enabled = True
        btnModifier.Enabled = True
        txtNomBalise.Enabled = True
        lblInformation.Visible = False
        MasquerInfo()
    End SUb
    ''' <summary>
    ''' Le bouton permet de trouver un élement qui est dans le listbox dans le TreeView
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnTrouverElem_Click(sender As Object, e As EventArgs) Handles btnTrouverElem.Click
        noeudCourant = CType(lboResultatRechercheBalise.SelectedItem, Balise)
        trArbreBalises.SelectedNode = noeudCourant
        trArbreBalises.HideSelection = True
        lblInformation.Visible = False
    End Sub

    Private Sub trArbreBalises_BeforeSelect(sender As Object, e As TreeViewCancelEventArgs) Handles trArbreBalises.BeforeSelect
        If trArbreBalises.SelectedNode IsNot Nothing Then
            trArbreBalises.SelectedNode.BackColor = System.Drawing.SystemColors.Window
            trArbreBalises.SelectedNode.ForeColor = System.Drawing.SystemColors.WindowText
        End If
    End Sub

    Private Sub lboResultatRechercheBalise_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lboResultatRechercheBalise.SelectedIndexChanged
        If lboResultatRechercheBalise.SelectedItem IsNot nothing
            btnTrouverElem.Enabled = True
            btnEffacer.Enabled = True
        Else
            btnTrouverElem.Enabled = False
            btnEffacer.Enabled = False
        End If
    End Sub

    Private Sub AffErreurBalise(bool As Boolean)
        lblInformation.Visible = True
        If bool = True
            lblInformation.Text = "Il n'y a aucune balise de sélectionnée!"
            lblInformation.ForeColor = Color.Red
        Else
            lblInformation.Text = "Le changement a bien été effectué!"
            lblInformation.ForeColor = Color.Green
        End If
    End Sub

    ''' <summary>
    ''' Permet de modifier les informations d'une balise.
    ''' </summary>
    Private Sub ModifierBalise()
        If trArbreBalises.SelectedNode IsNot Nothing

            Dim unDico As New Dictionary(Of String, String)
            noeudCourant = trArbreBalises.SelectedNode
            For index = 0 To dgvAffAttributContenu.Rows.Count -1
                Dim cle As String = dgvAffAttributContenu.Rows(index).Cells(0).Value
                Dim valeurCle As String = dgvAffAttributContenu.Rows(index).Cells(1).Value
                If Not String.IsNullOrEmpty(cle) AndAlso Not String.IsNullOrEmpty(valeurCle)
                    If Not unDico.ContainsKey(cle)
                        unDico.Add(cle,valeurCle)
                    End If
                End If
            Next
            AffErreurBalise(false)
            noeudCourant.Attributs = unDico
            noeudCourant.Contenu = rtxContenuBalise.Text
        Else
            AffErreurBalise(True)
        End If
    End Sub
    ''' <summary>
    ''' Le bouton permet d'effacer le contenu de la ListBox
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub BtnEffacer_Click(sender As Object, e As EventArgs) Handles btnEffacer.Click
        EffacerLbo()
    End Sub

    Private SUb EffacerLbo()
        lboResultatRechercheBalise.Items.Clear()
        btnTrouverElem.Enabled = False
        btnEffacer.Enabled = False
        txtNomRecherche.Text = ""
        MasquerInfo()
    End SUb
    ''' <summary>
    ''' permet de masquer certaines erreurs dans le programme
    ''' </summary>
    Private Sub MasquerInfo()
        lblErreurRecherche.Visible = False
        lblChargementFichier.Visible = False
    End Sub

    Private Sub btnRechercheId_Click(sender As Object, e As EventArgs) Handles btnRechercheId.Click
        lboResultatRechercheBalise.Items.Clear()
        If Not String.IsNullOrEmpty(txtNomRecherche.Text)
            RechercheSpecifique(noeudRacine, txtNomRecherche.Text,2)
            AffErreurRecherche(False)
        Else
            AffErreurRecherche(True)
        End If
    End Sub

End Class
