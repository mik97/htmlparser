﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPrincipal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.trArbreBalises = New System.Windows.Forms.TreeView()
        Me.btnModifier = New System.Windows.Forms.Button()
        Me.btnSupprimer = New System.Windows.Forms.Button()
        Me.btnAjouterBalise = New System.Windows.Forms.Button()
        Me.btnRechercherBalise = New System.Windows.Forms.Button()
        Me.txtNomRecherche = New System.Windows.Forms.TextBox()
        Me.btnRechercheId = New System.Windows.Forms.Button()
        Me.lboResultatRechercheBalise = New System.Windows.Forms.ListBox()
        Me.lblAffContenu = New System.Windows.Forms.Label()
        Me.lblContenu = New System.Windows.Forms.Label()
        Me.rtxContenuBalise = New System.Windows.Forms.RichTextBox()
        Me.lblAffErreur = New System.Windows.Forms.Label()
        Me.dgvAffAttributContenu = New System.Windows.Forms.DataGridView()
        Me.Attribut = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contenu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ChargerFIchierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChargerFichierToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExporterFichierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnTrouverElem = New System.Windows.Forms.Button()
        Me.txtNomBalise = New System.Windows.Forms.TextBox()
        Me.lblAttributs = New System.Windows.Forms.Label()
        Me.lblInformation = New System.Windows.Forms.Label()
        Me.lblChargementFichier = New System.Windows.Forms.Label()
        Me.lblErreurAttribut = New System.Windows.Forms.Label()
        Me.btnEffacer = New System.Windows.Forms.Button()
        Me.lblErreurRecherche = New System.Windows.Forms.Label()
        CType(Me.dgvAffAttributContenu,System.ComponentModel.ISupportInitialize).BeginInit
        Me.MenuStrip1.SuspendLayout
        Me.SuspendLayout
        '
        'trArbreBalises
        '
        Me.trArbreBalises.FullRowSelect = true
        Me.trArbreBalises.Location = New System.Drawing.Point(9, 32)
        Me.trArbreBalises.Margin = New System.Windows.Forms.Padding(2)
        Me.trArbreBalises.Name = "trArbreBalises"
        Me.trArbreBalises.Size = New System.Drawing.Size(162, 300)
        Me.trArbreBalises.TabIndex = 0
        '
        'btnModifier
        '
        Me.btnModifier.Enabled = false
        Me.btnModifier.Location = New System.Drawing.Point(504, 228)
        Me.btnModifier.Margin = New System.Windows.Forms.Padding(2)
        Me.btnModifier.Name = "btnModifier"
        Me.btnModifier.Size = New System.Drawing.Size(105, 47)
        Me.btnModifier.TabIndex = 3
        Me.btnModifier.Text = "Modifier Balise"
        Me.btnModifier.UseVisualStyleBackColor = true
        '
        'btnSupprimer
        '
        Me.btnSupprimer.Enabled = false
        Me.btnSupprimer.Location = New System.Drawing.Point(613, 229)
        Me.btnSupprimer.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSupprimer.Name = "btnSupprimer"
        Me.btnSupprimer.Size = New System.Drawing.Size(105, 47)
        Me.btnSupprimer.TabIndex = 4
        Me.btnSupprimer.Text = "Supprimer Balise"
        Me.btnSupprimer.UseVisualStyleBackColor = true
        '
        'btnAjouterBalise
        '
        Me.btnAjouterBalise.Enabled = false
        Me.btnAjouterBalise.Location = New System.Drawing.Point(504, 279)
        Me.btnAjouterBalise.Margin = New System.Windows.Forms.Padding(2)
        Me.btnAjouterBalise.Name = "btnAjouterBalise"
        Me.btnAjouterBalise.Size = New System.Drawing.Size(105, 47)
        Me.btnAjouterBalise.TabIndex = 6
        Me.btnAjouterBalise.Text = "Ajouter Balise:"
        Me.btnAjouterBalise.UseVisualStyleBackColor = true
        '
        'btnRechercherBalise
        '
        Me.btnRechercherBalise.Enabled = false
        Me.btnRechercherBalise.Location = New System.Drawing.Point(504, 11)
        Me.btnRechercherBalise.Margin = New System.Windows.Forms.Padding(2)
        Me.btnRechercherBalise.Name = "btnRechercherBalise"
        Me.btnRechercherBalise.Size = New System.Drawing.Size(94, 42)
        Me.btnRechercherBalise.TabIndex = 7
        Me.btnRechercherBalise.Text = "Rechercher Balise"
        Me.btnRechercherBalise.UseVisualStyleBackColor = true
        '
        'txtNomRecherche
        '
        Me.txtNomRecherche.Enabled = false
        Me.txtNomRecherche.Location = New System.Drawing.Point(704, 25)
        Me.txtNomRecherche.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNomRecherche.Name = "txtNomRecherche"
        Me.txtNomRecherche.Size = New System.Drawing.Size(76, 20)
        Me.txtNomRecherche.TabIndex = 9
        '
        'btnRechercheId
        '
        Me.btnRechercheId.Enabled = false
        Me.btnRechercheId.Location = New System.Drawing.Point(602, 11)
        Me.btnRechercheId.Margin = New System.Windows.Forms.Padding(2)
        Me.btnRechercheId.Name = "btnRechercheId"
        Me.btnRechercheId.Size = New System.Drawing.Size(94, 41)
        Me.btnRechercheId.TabIndex = 10
        Me.btnRechercheId.Text = "Recherche Id"
        Me.btnRechercheId.UseVisualStyleBackColor = true
        '
        'lboResultatRechercheBalise
        '
        Me.lboResultatRechercheBalise.FormattingEnabled = true
        Me.lboResultatRechercheBalise.HorizontalScrollbar = true
        Me.lboResultatRechercheBalise.Location = New System.Drawing.Point(504, 58)
        Me.lboResultatRechercheBalise.Margin = New System.Windows.Forms.Padding(2)
        Me.lboResultatRechercheBalise.Name = "lboResultatRechercheBalise"
        Me.lboResultatRechercheBalise.Size = New System.Drawing.Size(193, 121)
        Me.lboResultatRechercheBalise.TabIndex = 11
        '
        'lblAffContenu
        '
        Me.lblAffContenu.AutoSize = true
        Me.lblAffContenu.Location = New System.Drawing.Point(253, 50)
        Me.lblAffContenu.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblAffContenu.Name = "lblAffContenu"
        Me.lblAffContenu.Size = New System.Drawing.Size(0, 13)
        Me.lblAffContenu.TabIndex = 17
        '
        'lblContenu
        '
        Me.lblContenu.AutoSize = true
        Me.lblContenu.Location = New System.Drawing.Point(176, 16)
        Me.lblContenu.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblContenu.Name = "lblContenu"
        Me.lblContenu.Size = New System.Drawing.Size(53, 13)
        Me.lblContenu.TabIndex = 16
        Me.lblContenu.Text = "Contenu: "
        '
        'rtxContenuBalise
        '
        Me.rtxContenuBalise.Enabled = false
        Me.rtxContenuBalise.Location = New System.Drawing.Point(176, 32)
        Me.rtxContenuBalise.Margin = New System.Windows.Forms.Padding(2)
        Me.rtxContenuBalise.Name = "rtxContenuBalise"
        Me.rtxContenuBalise.Size = New System.Drawing.Size(295, 144)
        Me.rtxContenuBalise.TabIndex = 19
        Me.rtxContenuBalise.Text = ""
        '
        'lblAffErreur
        '
        Me.lblAffErreur.AutoSize = true
        Me.lblAffErreur.Location = New System.Drawing.Point(22, 293)
        Me.lblAffErreur.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblAffErreur.Name = "lblAffErreur"
        Me.lblAffErreur.Size = New System.Drawing.Size(0, 13)
        Me.lblAffErreur.TabIndex = 20
        '
        'dgvAffAttributContenu
        '
        Me.dgvAffAttributContenu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAffAttributContenu.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Attribut, Me.Contenu})
        Me.dgvAffAttributContenu.Enabled = false
        Me.dgvAffAttributContenu.Location = New System.Drawing.Point(176, 198)
        Me.dgvAffAttributContenu.Margin = New System.Windows.Forms.Padding(2)
        Me.dgvAffAttributContenu.Name = "dgvAffAttributContenu"
        Me.dgvAffAttributContenu.RowHeadersVisible = false
        Me.dgvAffAttributContenu.RowTemplate.Height = 24
        Me.dgvAffAttributContenu.Size = New System.Drawing.Size(294, 168)
        Me.dgvAffAttributContenu.TabIndex = 21
        '
        'Attribut
        '
        Me.Attribut.HeaderText = "Attribut"
        Me.Attribut.Name = "Attribut"
        '
        'Contenu
        '
        Me.Contenu.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Contenu.HeaderText = "Contenu"
        Me.Contenu.Name = "Contenu"
        Me.Contenu.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.Menu
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChargerFIchierToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(794, 24)
        Me.MenuStrip1.TabIndex = 22
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ChargerFIchierToolStripMenuItem
        '
        Me.ChargerFIchierToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChargerFichierToolStripMenuItem1, Me.ExporterFichierToolStripMenuItem})
        Me.ChargerFIchierToolStripMenuItem.Name = "ChargerFIchierToolStripMenuItem"
        Me.ChargerFIchierToolStripMenuItem.Size = New System.Drawing.Size(88, 20)
        Me.ChargerFIchierToolStripMenuItem.Text = "Menu Fichier"
        '
        'ChargerFichierToolStripMenuItem1
        '
        Me.ChargerFichierToolStripMenuItem1.Name = "ChargerFichierToolStripMenuItem1"
        Me.ChargerFichierToolStripMenuItem1.Size = New System.Drawing.Size(180, 22)
        Me.ChargerFichierToolStripMenuItem1.Text = "Charger Fichier"
        '
        'ExporterFichierToolStripMenuItem
        '
        Me.ExporterFichierToolStripMenuItem.Name = "ExporterFichierToolStripMenuItem"
        Me.ExporterFichierToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ExporterFichierToolStripMenuItem.Text = "Exporter Fichier"
        '
        'btnTrouverElem
        '
        Me.btnTrouverElem.Enabled = false
        Me.btnTrouverElem.Location = New System.Drawing.Point(700, 88)
        Me.btnTrouverElem.Margin = New System.Windows.Forms.Padding(2)
        Me.btnTrouverElem.Name = "btnTrouverElem"
        Me.btnTrouverElem.Size = New System.Drawing.Size(79, 37)
        Me.btnTrouverElem.TabIndex = 23
        Me.btnTrouverElem.Text = "Trouver Élément"
        Me.btnTrouverElem.UseVisualStyleBackColor = true
        '
        'txtNomBalise
        '
        Me.txtNomBalise.Enabled = false
        Me.txtNomBalise.Location = New System.Drawing.Point(620, 286)
        Me.txtNomBalise.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNomBalise.Name = "txtNomBalise"
        Me.txtNomBalise.Size = New System.Drawing.Size(76, 20)
        Me.txtNomBalise.TabIndex = 25
        '
        'lblAttributs
        '
        Me.lblAttributs.AutoSize = true
        Me.lblAttributs.Location = New System.Drawing.Point(176, 180)
        Me.lblAttributs.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblAttributs.Name = "lblAttributs"
        Me.lblAttributs.Size = New System.Drawing.Size(48, 13)
        Me.lblAttributs.TabIndex = 26
        Me.lblAttributs.Text = "Attributs:"
        '
        'lblInformation
        '
        Me.lblInformation.AutoSize = true
        Me.lblInformation.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblInformation.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblInformation.Location = New System.Drawing.Point(514, 338)
        Me.lblInformation.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblInformation.Name = "lblInformation"
        Me.lblInformation.Size = New System.Drawing.Size(0, 15)
        Me.lblInformation.TabIndex = 27
        Me.lblInformation.Visible = false
        '
        'lblChargementFichier
        '
        Me.lblChargementFichier.AutoSize = true
        Me.lblChargementFichier.ForeColor = System.Drawing.Color.Red
        Me.lblChargementFichier.Location = New System.Drawing.Point(95, 3)
        Me.lblChargementFichier.Name = "lblChargementFichier"
        Me.lblChargementFichier.Size = New System.Drawing.Size(178, 13)
        Me.lblChargementFichier.TabIndex = 28
        Me.lblChargementFichier.Text = "Le document n'a pas pu être chargé"
        Me.lblChargementFichier.Visible = false
        '
        'lblErreurAttribut
        '
        Me.lblErreurAttribut.AutoSize = true
        Me.lblErreurAttribut.Location = New System.Drawing.Point(224, 180)
        Me.lblErreurAttribut.Name = "lblErreurAttribut"
        Me.lblErreurAttribut.Size = New System.Drawing.Size(0, 13)
        Me.lblErreurAttribut.TabIndex = 29
        '
        'btnEffacer
        '
        Me.btnEffacer.Enabled = false
        Me.btnEffacer.Location = New System.Drawing.Point(700, 130)
        Me.btnEffacer.Name = "btnEffacer"
        Me.btnEffacer.Size = New System.Drawing.Size(79, 37)
        Me.btnEffacer.TabIndex = 30
        Me.btnEffacer.Text = "Effacer Recherche"
        Me.btnEffacer.UseVisualStyleBackColor = true
        '
        'lblErreurRecherche
        '
        Me.lblErreurRecherche.AutoSize = true
        Me.lblErreurRecherche.ForeColor = System.Drawing.Color.Red
        Me.lblErreurRecherche.Location = New System.Drawing.Point(504, 185)
        Me.lblErreurRecherche.Name = "lblErreurRecherche"
        Me.lblErreurRecherche.Size = New System.Drawing.Size(203, 13)
        Me.lblErreurRecherche.TabIndex = 31
        Me.lblErreurRecherche.Text = "Vous devez spécifier un mot à rechercher"
        Me.lblErreurRecherche.Visible = false
        '
        'FrmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(794, 396)
        Me.Controls.Add(Me.lblErreurRecherche)
        Me.Controls.Add(Me.btnEffacer)
        Me.Controls.Add(Me.lblErreurAttribut)
        Me.Controls.Add(Me.lblChargementFichier)
        Me.Controls.Add(Me.lblInformation)
        Me.Controls.Add(Me.lblAttributs)
        Me.Controls.Add(Me.txtNomBalise)
        Me.Controls.Add(Me.btnTrouverElem)
        Me.Controls.Add(Me.dgvAffAttributContenu)
        Me.Controls.Add(Me.lblAffErreur)
        Me.Controls.Add(Me.rtxContenuBalise)
        Me.Controls.Add(Me.lblAffContenu)
        Me.Controls.Add(Me.lblContenu)
        Me.Controls.Add(Me.lboResultatRechercheBalise)
        Me.Controls.Add(Me.btnRechercheId)
        Me.Controls.Add(Me.txtNomRecherche)
        Me.Controls.Add(Me.btnRechercherBalise)
        Me.Controls.Add(Me.btnAjouterBalise)
        Me.Controls.Add(Me.btnSupprimer)
        Me.Controls.Add(Me.btnModifier)
        Me.Controls.Add(Me.trArbreBalises)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "FrmPrincipal"
        Me.Text = "HTMLParser"
        CType(Me.dgvAffAttributContenu,System.ComponentModel.ISupportInitialize).EndInit
        Me.MenuStrip1.ResumeLayout(false)
        Me.MenuStrip1.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents trArbreBalises As TreeView
    Friend WithEvents btnModifier As Button
    Friend WithEvents btnSupprimer As Button
    Friend WithEvents btnAjouterBalise As Button
    Friend WithEvents btnRechercherBalise As Button
    Friend WithEvents txtNomRecherche As TextBox
    Friend WithEvents btnRechercheId As Button
    Friend WithEvents lboResultatRechercheBalise As ListBox
    Friend WithEvents lblAffContenu As Label
    Friend WithEvents lblContenu As Label
    Friend WithEvents rtxContenuBalise As RichTextBox
    Friend WithEvents lblAffErreur As Label
    Friend WithEvents dgvAffAttributContenu As DataGridView
    Friend WithEvents Attribut As DataGridViewTextBoxColumn
    Friend WithEvents Contenu As DataGridViewTextBoxColumn
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ChargerFIchierToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ChargerFichierToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ExporterFichierToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnTrouverElem As Button
    Friend WithEvents txtNomBalise As TextBox
    Friend WithEvents lblAttributs As Label
    Friend WithEvents lblInformation As Label
    Friend WithEvents lblChargementFichier As Label
    Friend WithEvents lblErreurAttribut As Label
    Friend WithEvents btnEffacer As Button
    Friend WithEvents lblErreurRecherche As Label
End Class
