﻿Imports System.Text
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports HTMLPARSER 
<TestClass()> Public Class BaliseTest

    <TestMethod()> Public Sub ConstructeurTest()
        Dim unDico As New Dictionary(Of String, String)
        Dim uneBalise As New Balise("Contenu de ma Balise","Html",unDico)
        Assert.IsNotNull(uneBalise)
        Assert.IsNotNull(uneBalise.NomBalise)
        Assert.IsNotNull(uneBalise.Contenu)
        Assert.AreEqual(uneBalise.Attributs.Count,0)
    End Sub

    <TestMethod()> Public Sub ToStringTest()
        Dim unDico As New Dictionary(Of String, String)
        Dim uneBalise As New Balise("Contenu de ma Balise","Html",unDico)
        Dim testString As String = uneBalise.ToString()
        Assert.IsNotNull(testString)
        Assert.AreNotEqual(testString,"<Html>Contenu de ma Balise")
        Assert.AreEqual(testString,"<html>Contenu de ma Balise")
    End Sub

End Class